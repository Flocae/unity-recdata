# Changelog

Tous les changemsnt notables sont documenté sur ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
#### Types de changements
- **Added** pour les nouvelles fonctionnalités.
- **Changed** pour les changements de fonctionnalités préexistantes.
- **Deprecated** pour les fonctionnalités qui seront bientôt supprimées.
- **Removed** pour les fonctionnalités désormais supprimées.
- **Fixed** pour les corrections de bugs.
Security en cas de vulnérabilités.

## [Unreleased]
- Modification d'une table de données existante (ajout ou suppression d'un colonne)
- Système de la fréquence d'enregistremet non fonctionnel. Un argument supplémentaire sera renseigné lors de l'enregistrement d'une variable : si celui-ci doit se faire en une seule fois ou de façon régulier (défini par un interval de temps)
- prefabs d'un nouveau formulaire avec script

## [0.7] - 2021-02-13
### Added
- Mise en place d'une function pour prendre des screenshot. Usage =>  DataMainScriptSingleton.Instance.controler.screenshot(name);

## [0.6] - 2020-05-03
### Added
- Mise en place d'un changelog
- Nouveau dossier "unity-recdata/assets/prefabs"
- Prefabs "RecManager" avec script principal (DataMainScriptSingleton.cs)

### Changed
- Réorganisation du dossier "Package", les anciennes versions du package sont sont dans le dossier "unity-recdata/package/Old"
- les classes de données spécifiques sont maintenant optionnelles (elle ne sont plus instanciés, mais un exemple est fourni dans DataMainScriptSingleton => instantiateSpeDataClass())
- Lisibilité des variables améliorées dans l'inspector
- possibilité ou non d'activité la sécurité (code et EOLdate)
- DontDestroyOnLoad attaché au recManager

### Deprecated

### Removed
- script RecDataForm.cs
- methode DataMainScriptSingleton.startExpe()

### Fixed
- Fix issue with inspector width in 2019

---
## *Template*

## [X.X] - YYYY-MM-DD
### Added
- XXX

### Changed
- XXX

### Deprecated
 - XXX

### Removed
- XXX

### Fixed
- XXX
