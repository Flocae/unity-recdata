﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Caetta.Lmc2.Data.DataClasses
{
    /// <summary>
    /// <para>Cette classe contient toutes les informations de l'application </para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class AppData : MainDataClass
    {
        private readonly string applicationName;
        private readonly string deviceName;
        private readonly string deviceModel;
        public AppData(bool insertData = true)
        {
            // Device & App info
            applicationName = Application.productName;
            deviceName = SystemInfo.deviceName;
            deviceModel = SystemInfo.deviceModel;
            //db
            this.tableName = "appdata";
            if (insertData)
            {
                this.InsertData();
            }
        }

        public override void MajInsertQuery()
        {
            this.insertQuery = "INSERT INTO " +
                this.tableName +
                " (" +
                "DateLancement," +
                "ApplicationName," +
                "DeviceName," +
                "DeviceModel" +
                ") VALUES ('" +
                this.DateLaunch +
                "','" +
                this.applicationName +
                "','"
                + this.deviceName +
                "','"
                + this.deviceModel +
                "')";
        }
        public override void MajUpdateByIDQuery(int UID)
        {
            this.insertQuery = "UPDATE " +
                this.tableName +
                " SET " +
                "DateLancement = " + this.DateLaunch +" ,"+
                "ApplicationName = " + this.applicationName + " ," +
                "DeviceName = " + this.deviceName + " ," +
                "DeviceModel =" + this.deviceModel + 
                " WHERE id =" + UID
                ;
        }

        
        ///<summary>
        /// Récupère nom application
        /// </summary>
        public string ApplicationName
        {
            get
            {
                return applicationName;
            }
        }
        ///<summary>
        /// Récupère le nom du device
        /// </summary>
        public string DeviceName
        {
            get
            {
                return deviceName;
            }
        }
        ///<summary>
        /// Récupère le model du device
        /// </summary>
        public string DeviceModel
        {
            get
            {
                return deviceModel;
            }
        }
    }
}

