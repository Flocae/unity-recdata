﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static DataMainScriptSingleton;
using System.IO;

// ██████╗  ██████╗     ███╗   ██╗ ██████╗ ████████╗    ████████╗ ██████╗ ██╗   ██╗ ██████╗██╗  ██╗    ██╗
// ██╔══██╗██╔═══██╗    ████╗  ██║██╔═══██╗╚══██╔══╝    ╚══██╔══╝██╔═══██╗██║   ██║██╔════╝██║  ██║    ██║
// ██║  ██║██║   ██║    ██╔██╗ ██║██║   ██║   ██║          ██║   ██║   ██║██║   ██║██║     ███████║    ██║
// ██║  ██║██║   ██║    ██║╚██╗██║██║   ██║   ██║          ██║   ██║   ██║██║   ██║██║     ██╔══██║    ╚═╝
// ██████╔╝╚██████╔╝    ██║ ╚████║╚██████╔╝   ██║          ██║   ╚██████╔╝╚██████╔╝╚██████╗██║  ██║    ██╗
// ╚═════╝  ╚═════╝     ╚═╝  ╚═══╝ ╚═════╝    ╚═╝          ╚═╝    ╚═════╝  ╚═════╝  ╚═════╝╚═╝  ╚═╝    ╚═╝

namespace Caetta.Lmc2.Data.DataClasses
{
    /// <summary>
    /// <para>Contreleur générale pour manipuler les Data (remplace les classe specifique si possible</para>
    /// <para>Florent Caetta 2020/04</para>
    /// </summary>
    ///
    public class Controler 
    {
        
        /* Liste de variables (table + Nom variable + valeur).
         * Un dictionnaire est utilisé l'identifiant dans le dictionnaire (Tkey) correspond au nom de la table + nom de la variable
        */
        private readonly Dictionary<string, object> dictionaryData;
        // dossier pour screenshot
        private string _outputFolder;

        public Controler()
        {
            if (Instance.debugInfo)
                Debug.Log("Data Controler instancié");
            dictionaryData = new Dictionary<string, object>();
            // dossier cible pour screenshot
            _outputFolder = Application.persistentDataPath + "/Screenshots/";

            if (!Directory.Exists(_outputFolder))
            {
                Debug.Log("création du dossier screenshot: " + _outputFolder);
                Directory.CreateDirectory(_outputFolder);             
            }

        }

        public void setVariable(string tableName, string variableName, object value)
        {
            try
            {
                if (dictionaryData.ContainsKey(tableName + "_" + variableName))
                {
                    dictionaryData[tableName + "_" + variableName] = value;
                    if (Instance.debugInfo)
                        Debug.Log("DictionaryData mis à jour pour la clé " + tableName + "_" + variableName + " avec la valeur "+ value);
                }
                else
                {
                    dictionaryData.Add(tableName + "_" + variableName, value);
                    if (Instance.debugInfo)
                        Debug.Log("Nouvelle entrée DictionaryData avec la clé " + tableName + "_" + variableName + " et la valeur " + value);
                }
            }
            catch
            {
                Debug.Log("Impossible de définir la variable " + tableName + "_" + variableName);
            }
            
        }

        public object getVariable(string tableName, string variableName)
        {
            try
            {
                if (dictionaryData.ContainsKey(tableName + "_" + variableName))
                {
                    return dictionaryData[tableName + "_" + variableName];
                }
                else
                {
                    Debug.Log("Impossible de trouver la variable " + tableName + "_" + variableName);
                    return null;
                }
            }
            catch 
            {
                Debug.Log("Erreur lors de la tentative de récupération de la variable " + tableName + "_" + variableName);
                return null;
            }
        }

        public void newEntry(string tableName)
        {
            try
            {
                string insertQuery = "INSERT INTO " +
                 tableName +
                 " (" +
                 "entryDate" +
                 ") VALUES ('"
                 + System.DateTime.Now +
                 "')";
                Instance.sqliteData.Query(insertQuery);
                if (Instance.debugInfo)
                    Debug.Log("Nouvelle entrée créer pour la Table " + tableName);
            }
            catch
            {
                Debug.Log("Impossible de créer une entrée pour la Table " + tableName);
            }
        }
            
        public void saveData(string tableName, string variableName, object value)
        {
            try
            {
                int lastInsertedID = Instance.sqliteData.LastInsertedID(tableName);
                string insertQuery = "UPDATE " +
                    tableName +
                    " SET " +
                    variableName + "='" + value +
                    "' WHERE id =" + lastInsertedID
                    ;
                Instance.sqliteData.Query(insertQuery);
                if (Instance.debugInfo)
                    Debug.Log(" Variable "+variableName+" sauvegardée dans la table " +tableName);
            }
            catch
            {
                Debug.Log("Aucune entrée trouvée pour la table " + tableName +" et variable "+variableName);
            }
            
        }

        public void screenshot(string name="")
        {
            try
            {
                Debug.Log("Screenshot");
                string timestamp = DateTime.Now.ToString("yyyyMMddTHHmmss");
                ScreenCapture.CaptureScreenshot(_outputFolder + name +"_"+timestamp+".png");
            }
            catch
            {
                Debug.Log("Screenshot impossible");
            }
        }


    }
}