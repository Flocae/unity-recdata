﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Caetta.Lmc2.Data.DataClasses
{

    /// <summary>
    /// <para>Cette classe contient toutes les globales de l'application </para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class GlobalData : MainDataClass
    {
        private List<int> trajetPositionX;
        private List<int> trajetPositionY;
        private List<int> trajetPositionZ;
        /* TODO : demander Alex
    private readonly int videoTrace;
    private readonly int screenshotCam;
    */
        public GlobalData()
        {
            trajetPositionX = new List<int>();
            trajetPositionY = new List<int>();
            trajetPositionZ = new List<int>();
            this.tableName = "globaldata";
            //TestFunction();
        }
        public override void MajInsertQuery()
        {
            /*TODO*/
        }
        /// <summary>
        /// Get/set le trajet position X
        /// </summary>
        public List<int> TrajetPositionX
        {
            get
            {
                return trajetPositionX;
            }
            set
            {
                trajetPositionX = value;
            }
        }
        /// <summary>
        /// Get/set trajet position X
        /// </summary>
        public List<int> TrajetPositionY
        {
            get
            {
                return trajetPositionY;
            }
            set
            {
                trajetPositionY = value;
            }
        }
        /// <summary>
        /// Get/set trajet position Y
        /// </summary>
        public List<int> TrajetPositionZ
        {
            get
            {
                return trajetPositionZ;
            }
            set
            {
                trajetPositionZ = value;
            }
        }
        /// <summary>
        /// Ajoute les valeurs à trajetPositionX trajetPositionY trajetPositionZ
        /// </summary>
        public void AddPositionXYZ(int px, int py, int pz)
        {
            trajetPositionX.Add(px);
            trajetPositionY.Add(py);
            trajetPositionY.Add(pz);
        }
        
    }
}