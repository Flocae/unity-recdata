﻿using UnityEngine;
using System;
using Caetta.Lmc2.Data.db;
namespace Caetta.Lmc2.Data
{
    /// <summary>
    /// <para>Cette classe contient les propriétés/fonctions communes aux classes data </para>
    /// <para>Florent Caetta 2019/09</para>
    /// </summary>
    public abstract class MainDataClass
    {
        //database
        protected SqliteData sqliteData;
        protected string tableName;
        protected string insertQuery;
        public MainDataClass()
        {
            this.DateLaunch = System.DateTime.Now;
            this.StartTime = Time.time;
            sqliteData = SqliteData.Instance;
        }
        ///<summary>
        /// get/set ID unique de l'instanciation de la classe
        /// </summary>
        public int ID { get; set; }
        ///<summary>
        /// Get/Set date instanciation de la classe
        /// </summary>
        public DateTime DateLaunch { get; set; }
        ///<summary>
        /// Get/Set Temps écoulé en seconde entre le lancement de l'app et l'instanciation de la classe
        /// </summary>
        public float StartTime { get; set; }
        ///<summary>
        /// Temps écoulé en seconde depuis l'instanciation de la classe
        /// </summary>
        public float TimeElapsed
        {
            get
            {
                return Time.time - this.StartTime;
            }
        }
        public virtual void MajInsertQuery()
        {

        }
        public virtual void MajUpdateByIDQuery(int UID)
        {

        }
        public virtual void UpdateThisData(string whichData, object value)
        {

        }

        

        ///<summary>
        /// Insert Data
        /// </summary>
        public virtual void InsertData()
        {
            MajInsertQuery();
            sqliteData.Query(this.insertQuery);
            this.ID = sqliteData.LastInsertedID(this.tableName);
            DataMainScriptSingleton.Instance.Trace("Inserted to "+this.tableName+" table with ID  : " + this.ID);
        }
        ///<summary>
        /// Uppdate table with ID
        /// </summary>
        public virtual void UpdateByID(int UID)
        {
            MajUpdateByIDQuery(UID);
            sqliteData.Query(this.insertQuery);
            DataMainScriptSingleton.Instance.Trace("updated " + this.tableName + " at ID  : " + UID);
        }
        ///<summary>
        /// Save variable with last inserted ID
        /// </summary>
        public virtual void SaveData(string whichData, object value)
        {
            UpdateThisData(whichData, value);
            sqliteData.Query(this.insertQuery);
            DataMainScriptSingleton.Instance.Trace("updated " + this.tableName + " at ID  : " + this.ID);
        }
    }
}