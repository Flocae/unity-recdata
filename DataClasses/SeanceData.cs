﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Caetta.Lmc2.Data.DataClasses
{
    /// <summary>
    /// <para>Cette classe contient toutes les informations de la séance en cours ainsi que les données recoltées</para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class SeanceData : MainDataClass
    {
        // Liste des variables séance
        private string seanceName;
        private List<string> currentObjectifs;
        private Dictionary<string, string> seanceDataStringDict;
        private Dictionary<string, int> seanceDataIntDict;// Dictionnaire avec des variables INT de la sénce

        // Variable exemple
        private int nClicMyButtonEx;

        public SeanceData()
        {
            
            currentObjectifs = new List<string>();


            seanceDataIntDict = new Dictionary<string, int>();
            seanceDataStringDict = new Dictionary<string, string>();

            this.tableName = "seancedata";

            /*this.FeedSeanceDataIntDict(new List<string>
            {
                "DuTotObjectifs",
                "nClickPerSeance",
                "nClickPerObj",
                "nClickInterfDepl",
                "nClickConsigneEcr",
                "nClickConsigneOral",
                "nClickMap",
                "nEntrMetro",
                "nEntrBus"
            });
            this.FeedSeanceDataStringDict(new List<string>
            {
                "DuBetweenObjectifs",
                "ObjSuccess",
                "ObjFail",
                "ObjOrder",
                "trajetPosX",
                "trajetPosY",
                "trajetPosZ",
                "trajetRotX",
                "trajetRotY",
                "trajetRotZ",
                "RefSubject"
            });
            */

            //TestFunction();
            this.InsertData();
            
        }
        public override void MajInsertQuery()
        {
            string seanceName = "maSeance";
            //sessionID
            this.insertQuery = "INSERT INTO " +
                 this.tableName +
                 " (" +
                 "SeanceName," +
                 "DateDebut" +
                 ") VALUES ('"
                 + seanceName +
                 "','"
                 + this.DateLaunch +
                 "')";
        }
        public override void MajUpdateByIDQuery(int UID)
        {
           

        }
        public override void UpdateThisData(string whichData, object value)
        {
            this.insertQuery = "UPDATE " +
                this.tableName +
                " SET " +
                whichData + " = " + value +
                " WHERE id =" + this.ID
                ;

        }
        

        ///<summary>
        /// Récupère/ajoute liste des objectifs
        /// </summary>
        public List<string> CurrentObjectifs
        {
            get
            {
                return currentObjectifs;
            }
            set
            {
                currentObjectifs = value;
            }
        }
        // Get/set variable Exemple
        public int NClicMyButtonEx
        {
            get
            {
                return nClicMyButtonEx;
            }
            set
            {
                nClicMyButtonEx = value;
            }
        }


        ///<summary>
        /// Ajoute un objectif dans la liste des objectifs => this.CurrentObjectifs
        /// </summary>
        public void AddObjectif(string objectif)
        {
            currentObjectifs.Add(objectif);
        }
        ///<summary>
        /// Récupère liste des objectifs
        /// </summary>
        public Dictionary<string, int> SeanceDataIntDict
        {
            get
            {
                return seanceDataIntDict;
            }
        }
        ///<summary>
        /// Initialise variables (list) avec valeur 0 
        /// </summary>
        public void FeedSeanceDataIntDict(List<string> listVar)
        {
            foreach (string var in listVar)
            {
                this.AddSeanceDataIntDict(var, 0);
            }
        }
        ///<summary>
        /// Initialise variables (list) avec valeur 0 
        /// </summary>
        public void FeedSeanceDataStringDict(List<string> listVar)
        {
            foreach (string var in listVar)
            {
                this.AddSeanceDataStringDict(var, "");
            }
        }
        ///<summary>
        /// Ajoute une clé string donnée et sa valeur
        /// </summary>
        public void AddSeanceDataIntDict(string key, int value)
        {
            seanceDataIntDict.Add(key, value);
        }
        public void AddSeanceDataStringDict(string key, string value)
        {
            seanceDataStringDict.Add(key, value);
        }



        //////// COMMENTAIRES/TEST
        private void TestFunction()
        {

            this.AddSeanceDataIntDict("valeurTest", 120);

            // Recuperation valeur du dictionnaire
            int result;
            seanceDataIntDict.TryGetValue("valeurTest", out result);
            Debug.Log("valeurTest ==> " + result);
            // Mise à jour des valeur du dictionnaire
            for (int i = 0; i < 10; i++)
            {
                Debug.Log(seanceDataIntDict["valeurTest"] = seanceDataIntDict["valeurTest"] + 1);
                
            }
        }
        /// <summary>
		/// Inserion infoSujet dans la base de donnée
		/// </summary>
        public override void InsertData()
        {
            //TODO
            //this.insertQuery = "INSERT INTO " + this.tableName + " (NameSubject,AgeSubject,SexeSubject,EducationLevelSubject,PathologySubject,RefSubject) VALUES ('" + this.nameSubject + "','" + this.ageSubject + "','" + this.sexeSubject + "','" + this.educationLevelSubject + "','" + this.pathologySubject + "','" + this.refSubject + "')";
            //this.ID = sqliteData.LastInsertedID("subjectinfo");
            base.InsertData();
        }

    }


}
    