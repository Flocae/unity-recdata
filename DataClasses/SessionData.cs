﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Caetta.Lmc2.Data.DataClasses
{
    /// <summary>
    /// <para>Cette classe contient toutes les informations de la session en cours ainsi que les données recoltées</para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class SessionData : MainDataClass
    {
        private int numSession;
        private string expeName;
        private string manipName;

        ///Debug.Log("XXXX3 "+gameObject.transform.position.x);

        public SessionData()
        {
            this.tableName = "sessiondata";
        }
        public override void MajInsertQuery()
        {

            this.insertQuery = "INSERT INTO " +
                this.tableName +
                " (" +
                "NumSession," +
                "TempsUtilisation," +
                "SubjectID," +
                "ExpeName" +
                "ManipName" +
                "RefSubject" +
                ") VALUES ('" +
                this.numSession +
                "','" +
                this.TimeElapsed +
                "','"+
                DataMainScriptSingleton.Instance.currentSubject.ID +
                "','"+
                this.expeName +
                "','"+
                this.manipName +
                "','" +
                DataMainScriptSingleton.Instance.currentSubject.RefSubject +
                "')";
        }

        public override void MajUpdateByIDQuery(int UID)
        {
            this.insertQuery = "UPDATE " +
                this.tableName +
                " SET " +
                "NumSession='" + this.numSession + "' ," +
                "TempsUtilisation=" + this.TimeElapsed + " ," +
                "SubjectID='" + DataMainScriptSingleton.Instance.currentSubject.ID + "' ," +
                "ExpeName='" + this.expeName + "' ," +
                "ManipName='" + this.manipName + "' ," +
                "RefSubject='" + DataMainScriptSingleton.Instance.currentSubject.RefSubject + "' " +
                " WHERE id =" + UID
                ;
        }

        /// <summary>
        /// Récupère/renseigne le numéro de la session
        /// </summary>
        public int NumSession
        {
            get
            {
                return numSession;
            }
            set
            {
                numSession = value;
            }
        }
        ///<summary>
        /// Récupère/determine le nom de l'experience
        /// </summary>
        public string ExpeName
        {
            get
            {
                return expeName;
            }

            set
            {
                expeName = value;
            }
        }
        ///<summary>
        /// Récupère/determine le nom de la manip
        /// </summary>
        public string ManipName
        {
            get
            {
                return manipName;
            }

            set
            {
                manipName = value;
            }
        }
  
    }
}
    