﻿namespace Caetta.Lmc2.Data.DataClasses
{
    /// <summary>
    /// <para>Cette classe contient toutes les informations de base du participant</para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class SubjectInfo : MainDataClass
	{
        private string nameSubject;
        private int ageSubject;
        private string sexeSubject;
        private string educationLevelSubject;
        private string pathologySubject;
        private string refSubject;
        public SubjectInfo()
        {
            this.tableName = "subjectinfo";
            
        }
        public override void MajInsertQuery()
        {
            this.insertQuery = "INSERT INTO " +
                this.tableName +
                " ( " +
                "NameSubject," +
                "AgeSubject," +
                "SexeSubject," +
                "EducationLevelSubject," +
                "PathologySubject," +
                "RefSubject" +
                ") VALUES ('" +
                this.nameSubject +
                "','" +
                this.ageSubject +
                "','" +
                this.sexeSubject +
                "','" +
                this.educationLevelSubject +
                "','" +
                this.pathologySubject +
                "','" +
                this.refSubject +
                "')";
        }

        public override void MajUpdateByIDQuery(int UID)
        {
            this.insertQuery = "UPDATE " +
                this.tableName +
                " SET " +
                "NameSubject='" + this.nameSubject + "' ," +
                "AgeSubject=" + this.ageSubject + " ," +
                "SexeSubject='" + this.sexeSubject + "' ," +
                "EducationLevelSubject='" + this.educationLevelSubject + "' ," +
                "PathologySubject='" + this.pathologySubject + "' ," +
                "RefSubject='" + this.refSubject +"' "+
                " WHERE id =" + UID
                ;
        }
        /// <summary>
        /// Récupère/renseigne le nom du sujet
        /// </summary>
        public string NameSubject
        {
            get
            {
                return nameSubject;
            }
            set
            {
                nameSubject = value;
            }
        }
        /// <summary>
        /// Récupère/renseigne l'age du sujet
        /// </summary>
        public int AgeSubject
        {
            get
            {
                return ageSubject;
            }
            set
            {
                ageSubject = value;
            }
        }
        /// <summary>
        /// Récupère/renseigne le sexe du sujet
        /// </summary>
        public string SexeSubject
        {
            get
            {
                return sexeSubject;
            }
            set
            {
                sexeSubject = value;
            }
        }
        /// <summary>
        /// Récupère/renseigne le niveau d'étude
        /// </summary>
        public string EducationLevelSubject
        {
            get
            {
                return educationLevelSubject;
            }
            set
            {
                educationLevelSubject = value;
            }
        }
        /// <summary>
        /// Récupère/renseigne la pathologie du sujet
        /// </summary>
        public string PathologySubject
        {
            get
            {
                return pathologySubject;
            }
            set
            {
                pathologySubject = value;
            }
        }
        /// <summary>
        /// Récupère/renseigne la reference du sujet
        /// </summary>
        public string RefSubject
        {
            get
            {
                return refSubject;
            }
            set
            {
                refSubject = value;
            }
        }
    }
}
