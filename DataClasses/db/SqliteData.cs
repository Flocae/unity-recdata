﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Data;
using Mono.Data.Sqlite;
using System.IO;
using System;
namespace Caetta.Lmc2.Data.db
{
    public class SqliteData
    {
       
        private string connection;
        // Singleton instance
        private static SqliteData instance;
        public static SqliteData Instance { get { return instance; } }

        public SqliteData(string db_name,TextAsset DatabaseFile)
        {
            if (instance != null && instance != this)
            {
                DataMainScriptSingleton.Instance.Trace("ATTENTION : utiliser qu'une seule instance SqliteData => SqliteData.Instance");
            }
            else
            {
                DataMainScriptSingleton.Instance.Trace("Création instance de SqliteData");
                instance = this;
            }

            // Create db
            connection = "URI=file:" + Application.persistentDataPath + "/" + db_name;
            // Open connection
            IDbConnection dbcon = new SqliteConnection(connection);
            dbcon.Open();
            // Create tables if not exists
            IDbCommand dbcmd;
            IDataReader reader;
            dbcmd = dbcon.CreateCommand();
            // TABLES
            if (DatabaseFile != null)
            {
                string[] dataDb = DatabaseFile.text.Split(new char[] { '\n' }); // split en fonction du nombre de ligne

                List<string> tablesNames = new List<string>();
                List<string> variablesNames = new List<string>();

                for (int i = 1; i < dataDb.Length; i++)
                {
                    string[] row = dataDb[i].Split(new char[] { ';' });
                    tablesNames.Add(row[0]);// list avec noms de tables
                    variablesNames.Add(row[1] + " " + row[2]);// list avec toutes les variables 
                }
                string currentTableName = "";
                foreach (var tableName in tablesNames)// boucle pour associer variables aux tables
                {
                    if (tableName != currentTableName)
                    {
                        string currentVariablesNames = "";
                        for (int i = tablesNames.IndexOf(tableName); i <= tablesNames.LastIndexOf(tableName); i++)
                        {
                            currentVariablesNames += ", " + variablesNames[i];
                        }
                        currentTableName = tableName;
                        string q_createCurrentTable = "CREATE TABLE IF NOT EXISTS " + currentTableName + " (id INTEGER PRIMARY KEY, entryDate STRING" + currentVariablesNames + ")";
                        dbcmd.CommandText = q_createCurrentTable;
                        Debug.Log(q_createCurrentTable);
                        using (reader = dbcmd.ExecuteReader()) { }// Automatically dispose
                    }
                }
            }
            // Close connection
            dbcon.Close();
        }

       
        // Public functions
        public int LastInsertedID(string table)
        {
            string selectMaxId = "SELECT MAX(id) from " + table;
            IDbConnection dbcon = new SqliteConnection(connection);
            IDbCommand dbcmd;
            dbcon.Open();
            dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = selectMaxId;
            object val = dbcmd.ExecuteScalar();
            int maxId = int.Parse(val.ToString());
            // Close connection
            dbcon.Close();
            return maxId;
        }
        public void Query(string commandText)
        {
            // Open connection
            IDbConnection dbcon = new SqliteConnection(connection);
            dbcon.Open();
            // Insert values in table
            IDbCommand cmnd = dbcon.CreateCommand();
            cmnd.CommandText = commandText;
            cmnd.ExecuteNonQuery();
            dbcon.Close();
        }

    }

}



