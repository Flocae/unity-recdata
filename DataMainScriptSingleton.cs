﻿using System.Collections.Generic;
using UnityEngine;
using Caetta.Lmc2.Data.DataClasses;
using Caetta.Lmc2.Data.db;
using Caetta.Lmc2.Utils;
using System;
using System.IO;

/// <summary>
/// <para> Classe principale pour la gestion des toutes les données /para>
/// <para> Patron de conception Singleton => Utiliser DataMainScriptSingleton.Instance.Method()/para>
/// <para>Florent Caetta 2019/07</para>
/// </summary>
public class DataMainScriptSingleton : MonoBehaviour
{
    // Public Variables - inspector
    [Header("Identification de l'expérience")]
    public string ExpeName = "my_expe";
    public string ManipName = "my_manip";
    [Space(10)]
    [Header("Gestion des données")]
    public string DatabaseName = "my_db";
    public TextAsset DatabaseFile;
    [Range(0.0f, 600.0f)]
    public float FrequenceRec;
    [Space(10)]
    [Header("Sécurité")]
    public bool SecureApp = false; 
    public string AppCode = "";
    public string EOLDate="2022-01-24";// YY-MM-DD
    [Space(10)]
    [Header("Paramètres optionnels")]
    public bool BuildAndroid;
    public bool debugInfo = true;
    public bool systemInfo = true;

  
    // Singleton instance
    private static DataMainScriptSingleton instance;

    // Publics Data Class
    // Classe générique
    public Controler controler;
    // classes data optionnelles (à adapter spécifiquement pur chaque projet si besoin
    public AppData appData;
    public SubjectInfo currentSubject;
    public SeanceData currentSeance;
    public SessionData currentSession;
    public GlobalData globalData;
    private float _timeLeft;
    
    // Database
    public SqliteData sqliteData;
    public static DataMainScriptSingleton Instance { get { return instance; } }
    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject); // Ce gameObject ne doit pas être détruit au chargement d'une nouvelle scène
        }
    }
    void Start()
    {
        // create new instance of data controler
        controler = new Controler();
        // App Secure
        if (SecureApp)
        {
            AppSecure.EOLDate = DateTime.Parse(EOLDate);
            AppSecure.appCode = AppCode;
            AppSecure.isBuildAndroid = BuildAndroid;
            AppSecure.CheckEOF();
        }

        // Frequence d'enregistrement
        _timeLeft = FrequenceRec;
        // Debug
        if (systemInfo){TraceSystemInfo();}
        // initialisation singleton db
        sqliteData = new SqliteData(DatabaseName, DatabaseFile);
        
    }
    void Update()
    {
        if (FrequenceRec != 0)
        {
            _timeLeft -= Time.deltaTime;
            if (_timeLeft < 0)
            {
                _timeLeft = FrequenceRec;
                Trace("Rec Now", false);
            }
        }
    }

    /*
      * AS3 DEV <3
      */
    private void TraceSystemInfo()
    {
        debugInfo = true;
        this.Trace("////INFOS SYSTEM////");
        this.Trace("Product Name => " + Application.productName);
        this.Trace("Device Name => " + SystemInfo.deviceName);
        this.Trace("Device Model => " + SystemInfo.deviceModel);
        this.Trace("Application.persistentDataPath => " + Application.persistentDataPath);
        this.Trace("System Date => " + System.DateTime.Now);
        
        this.Trace("__________________________");
    }

    public void Trace(string message, bool show = true)
    {
        if (debugInfo && show)
        {
            Debug.Log(message);
        }
    }

    /*
     * EXEMPLE FUNCTIONS  
     */

    private void instantiateSpeDataClass()
    {
        SubjectInfo monSujet = new SubjectInfo();

        // instantiation des differentes classes Data
        appData = new AppData(true);
        currentSeance = new SeanceData(); // Initialise une séance avec nouvel ID
        currentSession = new SessionData
        {
            ExpeName = this.ExpeName,
            ManipName = this.ManipName
        };
        globalData = new GlobalData();
    }
    private void TestSessionData()
    {
        // Test Info session
        currentSession.NumSession = 2;
        currentSeance.AddObjectif("tour eiffel");
        currentSeance.AddObjectif("Notre Dame");
        this.Trace("Num session : " + currentSession.NumSession);
        foreach (string obj in currentSeance.CurrentObjectifs)
        {
            Debug.Log(" Objectifs : " + obj);
        }
        this.Trace("Durée séance => " + currentSession.TimeElapsed);
        // Tests session Data ; permet d'ajouter n'importe quelle donnée
        currentSeance.AddSeanceDataIntDict("nClicTot", 67);
        currentSeance.AddSeanceDataIntDict("nClicDeplacement", 34);
        currentSeance.AddSeanceDataIntDict("nClicConsigneEcrite", 12);
        currentSeance.AddSeanceDataIntDict("nClicConsigneOral", 2);
        currentSeance.AddSeanceDataIntDict("nClickBetweenObj_1-2", 32);
        currentSeance.AddSeanceDataIntDict("nClickBetweenObj_2-3", 14);
        currentSeance.AddSeanceDataIntDict("nTimeMetro", 3);
        currentSeance.AddSeanceDataIntDict("nTimeBus", 3);
        foreach (KeyValuePair<string, int> keyValuePair in currentSeance.SeanceDataIntDict)
        {
            this.Trace(keyValuePair.Key + " =>" + keyValuePair.Value);
        }
    }
}

