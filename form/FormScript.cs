﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Caetta.Lmc2.Data.DataClasses;
using System;
using Caetta.Lmc2.Utils;

public class FormScript : MonoBehaviour {
    // UI text Input Subject info
    public InputField nomSujet;
    public Dropdown sexeSujet;
    public InputField ageSujet;
    public InputField educationSujet;
    public InputField groupSujet;
    public InputField referenceSujet;
    public Button startButton;
    public Button deleteButton;
    public Button testButton;
    public InputField codeApp;

    // Use this for initialization
    void Start () {
        FeedForm();
        // TODO : check TextInput is empty 
        startButton.onClick.AddListener(OnButtonStartClick);
        deleteButton.onClick.AddListener(OnButtonDelClick);
        testButton.onClick.AddListener(OnTestButtonClick);
    }
    // Remplissage formulaire avec les PlayerPrefs
    private void FeedForm()
    {
        nomSujet.text = PlayerPrefs.GetString("NomSujet");
        ageSujet.text = PlayerPrefs.GetInt("AgeSujet") > 0 ? PlayerPrefs.GetInt("AgeSujet").ToString() : "";
        sexeSujet.value = PlayerPrefs.GetInt("SexeSujet");
        educationSujet.text = PlayerPrefs.GetString("EducSujet");
        groupSujet.text = PlayerPrefs.GetString("GroupSujet");
        referenceSujet.text = PlayerPrefs.GetString("RefSujet");
        codeApp.text = PlayerPrefs.GetString("AppCode");
    }
    private void OnButtonStartClick()
    {

       
        deleteButton.onClick.RemoveAllListeners();
       // testButton.onClick.RemoveAllListeners();
        // Save playersPrefs
        PlayerPrefs.SetString("NomSujet", nomSujet.text);
        PlayerPrefs.SetInt("AgeSujet", Convert.ToInt32(ageSujet.text));
        PlayerPrefs.SetInt("SexeSujet", sexeSujet.value);
        PlayerPrefs.SetString("EducSujet", educationSujet.text);
        PlayerPrefs.SetString("GroupSujet", groupSujet.text);
        PlayerPrefs.SetString("RefSujet", referenceSujet.text);
        PlayerPrefs.SetString("AppCode", codeApp.text);
        //
        // security
        AppSecure.CheckCode(PlayerPrefs.GetString("AppCode"));
        // Création nouvelle instance de "SubjectInfo" avec les informations du formulaire
        DataMainScriptSingleton.Instance.currentSubject = new SubjectInfo
        {
            NameSubject = nomSujet.text,
            SexeSubject = sexeSujet.options[sexeSujet.value].text,
            AgeSubject = Convert.ToInt32(ageSujet.text),
            EducationLevelSubject = educationSujet.text,
            PathologySubject = groupSujet.text,
            RefSubject = referenceSujet.text
        };
        DataMainScriptSingleton.Instance.currentSubject.InsertData();
        


        //deactivate form
        GameObject formCanva = GameObject.Find("form");
        formCanva.SetActive(false);
    }
    private void OnButtonDelClick()
    {
        PlayerPrefs.DeleteAll();
        nomSujet.text = ageSujet.text = educationSujet.text = groupSujet.text = referenceSujet.text = "";
        sexeSujet.value = -1;
    }
    private void OnTestButtonClick()
    {
        Debug.Log(DataMainScriptSingleton.Instance.currentSubject.NameSubject);

    }
}
