﻿using UnityEngine;
using UnityEngine.UI;
using Caetta.Lmc2.Data.DataClasses;
using static DataMainScriptSingleton;
using System;


public class TestButton : MonoBehaviour {

    public Button myButton;
    void Start () {
        // Vérifie qu'une instance SeanceData est bien disponible
       /* if (DataMainScriptSingleton.Instance.currentSeance==null)
		{
			DataMainScriptSingleton.Instance.currentSeance = new SeanceData();
		}
        */
        /* Ajoute une variable "nombre de clic sur ce bouton" avec 0 
         * comme valeur au dictionnaire de seanceDataIntDict la classe SeanceData*/
        //DataMainScriptSingleton.Instance.currentSeance.AddSeanceDataIntDict("nClicMyButton", 0);
		// Ecoute un évenement clic sur le bouton
		myButton.onClick.AddListener(OnMyButtonClick);
	}
	private void OnMyButtonClick()
    {
        // Incrémente la valeur de nClicMyButton
        //DataMainScriptSingleton.Instance.currentSeance.SeanceDataIntDict["nClicMyButton"] += 1;
        // récupération et affichage du nombre de clic sur la console
        //int nClic;
        // DataMainScriptSingleton.Instance.currentSeance.SeanceDataIntDict.TryGetValue("nClicMyButton", out nClic);
        // Debug.Log("Nombre de clic => " + nClic);
        // Incrémenter la valeur de nClicMyButtonEx
        //  DataMainScriptSingleton.Instance.currentSeance.NClicMyButtonEx += 1;
        // récupération et affichage de nClicMyButtonEx sur la console
        //  Debug.Log("NClicMyButtonEx = " + DataMainScriptSingleton.Instance.currentSeance.NClicMyButtonEx);
        //DataMainScriptSingleton.Instance.controler.saveData("testTable", "testVariable", "OK");



        // Gestion des variables en mémoire
        // Définir des valeurs
        Instance.controler.setVariable("TestTable", "testVarInt", 999); // La valeur de TestVarInt, variable associée à la table TestTable est 999 
        Instance.controler.setVariable("TestTable", "testVarText", "Alexandre"); // Le texte de la variable testVarText, variable associée à la table TestTabl, est "Alexandre"
        Instance.controler.setVariable("TestTable", "testVarFloat", 9.9); // La valeur de TestVarInt, variable associée à la table TestTable est 9.9
        // Récupérer les valeurs et les typer si necessaire
        int testVarInt=Convert.ToInt32(Instance.controler.getVariable("TestTable", "testVarInt"));
        Debug.Log("testVarInt => " + testVarInt);
        string testVarText = (Instance.controler.getVariable("TestTable", "testVarText")).ToString();
        Debug.Log("testVarString => " + testVarText);
        double testVarFloat = Convert.ToDouble(Instance.controler.getVariable("TestTable", "testVarFloat"));
        Debug.Log("testVarFloat => " + testVarFloat);

        // Gestion des variables dans la base de donnée
        Instance.controler.newEntry("testTable"); // Création d'une nouvelle entrée (ligne) pour la table "testTable
        Instance.controler.saveData("TestTable", "testVarInt", 999);// Sauvegarde de TestVarInt dans la BDD, variable associée à la table TestTable , avec une valeur de 999 
        Instance.controler.saveData("TestTable", "testVarText", "Alexandre");// Sauvegarde de testVarText dans la BDD, variable associée à la table TestTabl, avec comme entrée texte "Alexandre" 
        Instance.controler.saveData("TestTable", "testVarFloat", 9.9);// Sauvegarde de testVarFloat, variable associée à la table TestTable , avec une valeur de 9.9 

    }
}
