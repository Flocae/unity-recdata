﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace Caetta.Lmc2.Utils
{
    /// <summary>
    /// <para>Cette classe contient les fonctions liées à la sécurité de l'APP </para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    ///
    public static class AppSecure
    {
        public static DateTime EOLDate;
        public static string appCode;
        public static bool isBuildAndroid;

        public static void CheckCode(string code)
        {
            if (code != appCode)
            {
                Debug.Log("CodeAppQuit");
                CloseApp();
            }   
        }
        public static void CheckEOF()
        {
            if (DateTime.Compare(EOLDate, DateTime.Now) == -1)
            {
                Debug.Log("DateQuit");
                CloseApp();
            }
        }
        
        static void CloseApp()
        {
            // mettre ici si passage en build
            if(isBuildAndroid)
            {
                Application.Quit();     
            }
            else
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }      
        }
        
    }
}

