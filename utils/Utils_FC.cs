﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
namespace Caetta.Lmc2.Utils
{
    /// <summary>
    /// <para>Cette classe contient des functions </para>
    /// <para>Florent Caetta 2019/07</para>
    /// </summary>
    public class Utils
    {
        ///<summary>
        /// <para>Conversion int[] en string[]</para>
        /// <para>Usage : Utils_FC.ConvertArrayToString(ARRAY,"'")</para>
        /// </summary>
        public static string IntArrToStr(int[] arr, string separator = ",")
        {
            string result = String.Join(separator, new List<int>(arr).ConvertAll(i => i.ToString()).ToArray());
            return result;
        }
        public static string StrArrToStr(string[] arr, string separator = ",")
        {
            string result = String.Join(separator, new List<string>(arr).ConvertAll(i => i).ToArray());
            return result;
        }
        public static string StrListToStr(List<string> list, string separator = ",")
        {
            return string.Join(",", list.ToArray());
        }

        public static string IntListToStr(List<int> list, string separator = ",")
        {
            string[] array = new string[list.Count];
            int i = 0;
            foreach (var item in list)
            {
                array[i] = item.ToString();
                i++;
            }
            return string.Join(",", array);
        }



        // Queries maker DONOTUSE
        private static string INSERTOLD(string tableName, NameValueCollection valuesCollection)
        {
            string queryString = "INSERT INTO " + tableName + " (";
            for (int i = 0; i < valuesCollection.Count; i++)
            {
                queryString += valuesCollection.Keys[i] + (i + 1 == valuesCollection.Count ? "" : ",");
            }
            queryString += ") VALUES (";

            for (int i = 0; i < valuesCollection.Count; i++)
            {
                //queryString += Escape(valuesCollection[valuesCollection.Keys[i]]) + (i + 1 == valuesCollection.Count ? ("") : (","));
            }
            queryString += ");";
            return queryString;
        }
        
    }
}

